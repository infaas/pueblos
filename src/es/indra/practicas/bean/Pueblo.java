package es.indra.practicas.bean;

/**
 * Bean del objeto pueblo que identifica los valores de una ubicación
 * @author fjnogueras
 *
 */
public class Pueblo {
	/**
	 * id unico que identifica el pueblo
	 */
	protected int idPueblo;
	
	/**
	 * nombre del pueblo
	 */
	protected String nbPueblo;
	
	/**
	 * coordenadas gps que identifican la posición del pueblo
	 */
	protected double[] posPueblo; 
	
	
	/**
	 * contructor vacio
	 */
	public Pueblo() {
	}
	
	/**
	 * constructor del pueblo
	 * @param idPueblo
	 * @param nbPueblo
	 */
	public Pueblo(int idPueblo, String nbPueblo) {
		this.idPueblo = idPueblo;
		this.nbPueblo = nbPueblo;
	}
}
