package es.indra.practicas.pueblos.contrato;

import java.util.List;

import es.indra.practicas.bean.Pueblo;

public interface LosPueblos {
	
	/**
	 * Metodo que guardar� el pueblo con el que queremos trabajar
	 * @param pueblo
	 */
	public void setPueblo(Pueblo pueblo);

	/**
	 * Metodo que devuelve un pueblo determinado
	 * @param idPueblo identificador del pueblo
	 * @return objeto pueblo
	 */
	public Pueblo getPueblo(int idPueblo);
	
	/**
	 * Listado de idiomas que se hablan en el pueblo
	 * @return
	 */
	public List<String> getIdiomasDelPueblo();
	
	/**
	 * distancia entre 2 pueblos
	 * @param pueblo1 objeto que representa al primer pueblo
	 * @param pueblo2 objeto que representa al segundo pueblo
	 * @return distancia entre kilometros entre pueblos
	 */
	public double distanciaConOtroPueblo(Pueblo otroPueblo);
	
	/**
	 * Metodo que te dice si un pueblo es mas grande que otro 
	 * @param pueblo
	 * @return
	 */
	public boolean isPuebloMasGrande(Pueblo otroPueblo);
	
	
	
}
